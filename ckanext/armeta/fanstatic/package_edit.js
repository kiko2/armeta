$( document ).ready(function(){
	let key = "";
	let val = "";
	let i = 0;
	$("[id^='field-extras-'][id$='key']").each(function() {
		if ( $(this).val() != "on" && $(this).val().length != 0 ) {
			key = $(this).val();
			i++;
			val = $("#"+'field-extras-'+i+'-value').val();
			if (val.length != 0) {
				$("#field-"+key).val(val);
			}
			
		}

	});
	$('div[data-module="custom-fields"]').hide();
});