# coding: utf8
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit

import os
#from urllib.request import urlopen
from urllib2 import urlopen
import json
import re

def frequency_values():
    freq = []
    path_json_file = os.path.dirname(os.path.abspath(__file__))+'/frequencies.json'
    with open(path_json_file) as f:
        v_json = json.load(f)
    
    for v_record in v_json["frequencies"]:
        v_element = {
            "value": v_record["value"],
            "text": v_record["text"]["fr"]+'  ---  '+v_record["text"]["ar"]
        }
        freq.append(v_element) 
    return freq

def national_themes():
    themes = []
    path_json_file = os.path.dirname(os.path.abspath(__file__))+'/nationalthemes.json'
    with open(path_json_file) as f:
        v_json = json.load(f)
    
    for v_record in v_json["themes"]:
        v_element = {
            "value": v_record["ckan_id"],
            "text": v_record["text"]["fr"]+'  ---  '+v_record["text"]["ar"]
        }
        themes.append(v_element) 
    return themes

def geoLevels():
    levels = [
        {
            'value': u'national',
            'text' : u'National - وطني'
        },
        {
            'value': u'district',
            'text' : u'District - إقليمي'
        },
        {
            'value': u'gouvernorat',
            'text' : u'Gouvernorat - ولاية'
        },
        {
            'value': u'delegation',
            'text' : u'Délégation - معتمديّة'
        },
        {
            'value': u'secteur',
            'text' : u'Secteur - منطقة أو عمادة'
        },
        {
            'value': u'commune',
            'text' : u'Commune - بلديّة'
        },
        {
            'value': u'arrondissement',
            'text' : u'Arrondissement - دائرة بلديّة'
        },
        {
            'value': u'notspecified',
            'text': u'Information non spécifiée'
        }
    ]
    return levels

def locationTypes():
    locations = [
        {
            'value': u'geometry',
            'text' : u'Forme geometrique (geometry)'
        },
        {
            'value': u'boundingbox',
            'text' : u'Forme encadrante (bounding box)'
        },
        {
            'value': u'centroid',
            'text' : u'Point de localisation (centroid)'
        },
        {
            'value': u'notspecified',
            'text': u'Information non spécifiée'
        }
    ]
    return locations

def ifInSet(attributeVal, elementVal):
    if type(elementVal) is not str and type(elementVal) is not list:
        elementVal= elementVal.encode('utf-8')
    if type(attributeVal) is not str and type(attributeVal) is not list:
        attributeVal=attributeVal.encode('utf-8')
    if elementVal in attributeVal:
        return True
    return False

def notEmpty(value):
    if not value:
        raise toolkit.Invalid("Ce champ est obligatoire!")
    return value

def dateISO(value):
    r = re.compile('.{4}/.{2}/.{2}')
    if not (len(value)==10 and r.match(value)):
        raise toolkit.Invalid("Ce champ doit correspondre au formatage YYYY/MM/DD exemple : 2022/12/31")
    return value


class ArmetaPlugin(plugins.SingletonPlugin, toolkit.DefaultDatasetForm):
    plugins.implements(plugins.IConfigurable, inherit=True)
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IDatasetForm)
    plugins.implements(plugins.ITemplateHelpers)
    #plugins.implements(plugins.IPackageController, inherit=True)
    #plugins.implements(plugins.IMapper, inherit=True)
    #plugins.implements(plugins.IFacets, inherit=True)
    #plugins.implements(plugins.IValidators)


    def write_custom_package_schema(self, schema):
        schema.update({
            'title_ar': [toolkit.get_converter('convert_to_extras'), notEmpty, toolkit.get_validator('ignore_missing')],
	        'notes_ar': [toolkit.get_converter('convert_to_extras'), notEmpty, toolkit.get_validator('ignore_missing')],
            'national_theme': [toolkit.get_converter('convert_to_extras'), notEmpty, toolkit.get_validator('ignore_missing')],
            'temporal_frequency': [toolkit.get_converter('convert_to_extras'), notEmpty, toolkit.get_validator('ignore_missing')],
            'temporal_startDate': [toolkit.get_converter('convert_to_extras'), dateISO, toolkit.get_validator('ignore_missing')],
            'temporal_endDate': [toolkit.get_converter('convert_to_extras'), dateISO, toolkit.get_validator('ignore_missing')],
            'spatial_geoLevel': [toolkit.get_converter('convert_to_extras'), notEmpty, toolkit.get_validator('ignore_missing')],
            'spatial_geoName': [toolkit.get_converter('convert_to_extras'), toolkit.get_validator('ignore_missing')],
            'spatial_locationType': [toolkit.get_converter('convert_to_extras'), toolkit.get_validator('ignore_missing')],
            'spatial_locationValue': [toolkit.get_converter('convert_to_extras'), toolkit.get_validator('ignore_missing')],
            'tag_string_ar': [toolkit.get_converter('convert_to_extras'), notEmpty, toolkit.get_validator('ignore_missing')],
            'tag_string_fr': [toolkit.get_converter('convert_to_extras'), notEmpty, toolkit.get_validator('ignore_missing')]
            

        })
        
        schema['resources'].update({
                'name_ar': [toolkit.get_validator('ignore_missing')],
                'description_ar': [toolkit.get_validator('ignore_missing')]
        })
        
        return schema
    
    def read_custom_package_schema(self, schema):
        schema.update({
            'title_ar': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
	        'notes_ar': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'national_theme': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'temporal_frequency': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'temporal_startDate': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'temporal_endDate': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'spatial_geoLevel': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'spatial_geoName': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'spatial_locationType': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'spatial_locationValue': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'tag_string_ar': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')],
            'tag_string_fr': [toolkit.get_converter('convert_from_extras'), toolkit.get_validator('ignore_missing')]
        })
        
        schema['resources'].update({
                'name_ar': [toolkit.get_validator('ignore_missing')],
                'description_ar': [toolkit.get_validator('ignore_missing')]
        })
        
        return schema
    
    def create_package_schema(self):
        # let's grab the default schema in our plugin
        schema = super(ArmetaPlugin, self).create_package_schema()
        schema = self.write_custom_package_schema(schema)
        return schema

    def update_package_schema(self):
        # let's grab the default schema in our plugin
        schema = super(ArmetaPlugin, self).update_package_schema()
        schema = self.write_custom_package_schema(schema)       
        return schema

    def show_package_schema(self):
	# let's grab the default schema in our plugin
        schema = super(ArmetaPlugin, self).show_package_schema()
        schema = self.read_custom_package_schema(schema)
        return schema


    def is_fallback(self):
        return True

    def package_types(self):
        return []

    # IConfigurer

    def configure(self, config):
        toolkit.add_resource('fanstatic', 'ckanext-armeta')
    	
    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'armeta')


    def get_helpers(self):
        return {
            'frequencies': frequency_values,
            'national_themes': national_themes,
            'geoLevels':geoLevels,
            'locationTypes':locationTypes,
            'ifInSet': ifInSet
        }
